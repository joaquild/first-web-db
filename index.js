const express = require('express');
const client = require('mongodb').MongoClient;

const PORT = process.env.PORT || 5050

let app = express();
app.set('view engine', 'pug');
app.use(express.urlencoded({extended: true}));

async function GetData(){
    let connection = await client.connect('mongodb+srv://root:root@first-web-db.hxflh.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {useNewUrlParser: true});
    let root = connection.db('first-web-db');
    
    return await root.collection('first-web-db-collection').find({}).toArray();
}

async function InsertData(name){
    let connection = await client.connect('mongodb+srv://root:root@first-web-db.hxflh.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {useNewUrlParser: true});
    let root = connection.db('first-web-db');
    
    await root.collection('first-web-db-collection').insertOne({name})
}

app.get('/', function (req, res){
    (async function (){
       let dbdata = await GetData();
       res.render('index', {complaints});
    })
});


app.post('/new', function (req, res){
    (async function(){
        await InsertData(req.body.name);
        res.redirect('/')
    })();
});

app.listen(PORT, function(){
    console.log("Server listening on port " + PORT);
});
